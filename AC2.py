from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Se o senhor estiver vendo isso é porque funcionou, tenha um bom dia'

app.run(host='0.0.0.0', port=3000)